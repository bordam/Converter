# GERMAN translation for Converter.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the converter package.
# Jürgen Benvenuti <gastornis@posteo.org>, 2022-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: converter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 22:16-0600\n"
"PO-Revision-Date: 2023-01-05 16:02+0100\n"
"Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>\n"
"Language-Team: German <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: data/io.gitlab.adhami3310.Converter.desktop.in:3
#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:5
#: converter/gtk/window.blp:9 converter/gtk/window.blp:43
msgid "Converter"
msgstr "Converter"

#: data/io.gitlab.adhami3310.Converter.desktop.in:4
msgid "Convert"
msgstr "Umwandeln"

#: data/io.gitlab.adhami3310.Converter.desktop.in:5
msgid "Convert and Manipulate images"
msgstr "Bilder umwandeln und bearbeiten"

#: data/io.gitlab.adhami3310.Converter.desktop.in:11
msgid "image;convert;converting;processing"
msgstr ""
"image;convert;converting;processing;Bild;umwandeln;Umwandlung;konvertieren;"
"verarbeiten;"

#: data/io.gitlab.adhami3310.Converter.gschema.xml:6
msgid "Shows less popular datatypes in the menu"
msgstr "Weniger gängige Datentypen im Menü anzeigen"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:6
msgid "Khaleel Al-Adhami"
msgstr "Khaleel Al-Adhami"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:7
msgid "Convert and manipulate images"
msgstr "Bilder umwandeln und bearbeiten"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:13
msgid ""
"A simple and easy-to-use Flatpak application built with libadwaita for "
"converting and manipulating images."
msgstr ""
"Eine einfache und leicht zu bedienende Flatpak-Anwendung zum Umwandeln und "
"Bearbeiten von Bildern, erstellt mit Libadwaita."

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:26
msgid "image"
msgstr "Bild"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:27
msgid "convert"
msgstr "Umwandeln"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:28
msgid "converting"
msgstr "Umwandlung läuft"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:29
msgid "processing"
msgstr "Verarbeitung läuft"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:45
msgid "Overview"
msgstr "Übersicht"

#: data/io.gitlab.adhami3310.Converter.metainfo.xml.in:65
msgid "Made UI adjustments."
msgstr "Anpassungen an der Benutzeroberfläche vorgenommen."

#: converter/file_chooser.py:67
#, python-brace-format
msgid "’{input_ext}’ is not supported"
msgstr "»{input_ext}« wird nicht unterstützt"

#: converter/file_chooser.py:90
msgid "Select an image"
msgstr "Ein Bild auswählen"

#: converter/file_chooser.py:97
msgid "Supported image files"
msgstr "Unterstützte Bilddateien"

#: converter/file_chooser.py:114
msgid "No file extension was specified"
msgstr "Es wurde keine Dateiendung angegeben"

#: converter/file_chooser.py:120
#, python-brace-format
msgid "’{file_ext}’ is of the wrong format"
msgstr "»{file_ext}« hat das falsche Format"

#: converter/file_chooser.py:129
msgid "Select output location"
msgstr "Ausgabeort auswählen"

#: converter/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Allgemein"

#: converter/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Tastenkürzel anzeigen"

#: converter/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Quit"
msgstr "Beenden"

#: converter/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Open Image"
msgstr "Bild öffnen"

#: converter/gtk/window.blp:44
msgid "Click “Open Image…” or drag an image here."
msgstr "Klicken Sie auf »Bild öffnen …« oder ziehen Sie ein Bild hierher."

#: converter/gtk/window.blp:55
msgid "_Open Image…"
msgstr "Bild ö_ffnen …"

#: converter/gtk/window.blp:88
msgid "Drop Here to Open"
msgstr "Hier ablegen zum Öffnen"

#: converter/gtk/window.blp:110
msgid "Properties"
msgstr "Eigenschaften"

#: converter/gtk/window.blp:113
msgid "Image Type"
msgstr "Bildtyp"

#: converter/gtk/window.blp:120
msgid "Image Size"
msgstr "Bildgröße"

#: converter/gtk/window.blp:129
msgid "Options"
msgstr "Optionen"

#: converter/gtk/window.blp:161
msgid "Convert to"
msgstr "Umwandeln in"

#: converter/gtk/window.blp:166
msgid "Compress as"
msgstr "Komprimieren als"

#: converter/gtk/window.blp:179
msgid "More Options"
msgstr "Weitere Optionen"

#: converter/gtk/window.blp:188
msgid "_Convert"
msgstr "_Umwandeln"

#: converter/gtk/window.blp:208
msgid "Could not Load Image"
msgstr "Bild konnte nicht geladen werden"

#: converter/gtk/window.blp:209
msgid "This image could be corrupted or may use an unsupported file format."
msgstr ""
"Dieses Bild könnte beschädigt sein oder ein nicht unterstütztes Dateiformat "
"verwenden."

#: converter/gtk/window.blp:224
msgid "Quality"
msgstr "Qualität"

#: converter/gtk/window.blp:243
msgid "Background Color"
msgstr "Hintergrundfarbe"

#: converter/gtk/window.blp:251
msgid "DPI"
msgstr "DPI"

#: converter/gtk/window.blp:262
msgid "Scale Preserving Ratio"
msgstr "Seitenverhältnis beim Skalieren beibehalten"

#: converter/gtk/window.blp:265
msgid "Dimension"
msgstr "Abmessungen"

#: converter/gtk/window.blp:268 converter/gtk/window.blp:312
#: converter/gtk/window.blp:338
msgid "Width"
msgstr "Breite"

#: converter/gtk/window.blp:268 converter/gtk/window.blp:327
#: converter/gtk/window.blp:348
msgid "Height"
msgstr "Höhe"

#: converter/gtk/window.blp:273
msgid "Width in Pixels"
msgstr "Breite in Pixeln"

#: converter/gtk/window.blp:283
msgid "Height in Pixels"
msgstr "Höhe in Pixeln"

#: converter/gtk/window.blp:295
msgid "Resize"
msgstr "Größe ändern"

#: converter/gtk/window.blp:298
msgid "Filter"
msgstr "Filter"

#: converter/gtk/window.blp:304
msgid "Size"
msgstr "Größe"

#: converter/gtk/window.blp:307
msgid "Percentage"
msgstr "Prozent"

#: converter/gtk/window.blp:307
msgid "Exact Pixels"
msgstr "Genaue Pixelanzahl"

#: converter/gtk/window.blp:307
msgid "Minimum Pixels"
msgstr "Mindestpixelanzahl"

#: converter/gtk/window.blp:307
msgid "Maximum Pixels"
msgstr "Höchstpixelanzahl"

#: converter/gtk/window.blp:307
msgid "Ratio"
msgstr "Seitenverhältnis"

#: converter/gtk/window.blp:315
msgid "Pixels"
msgstr "Pixel"

#: converter/gtk/window.blp:315
msgid "Preserve Ratio"
msgstr "Seitenverhältnis beibehalten"

#: converter/gtk/window.blp:358
msgid "Width Percentage Scale"
msgstr "Prozentwert Breitenskalierung"

#: converter/gtk/window.blp:368
msgid "Height Percentage Scale"
msgstr "Prozentwert Höhenskalierung"

#: converter/gtk/window.blp:378
msgid "Ratio Width"
msgstr "Seitenverhältnis Breite"

#: converter/gtk/window.blp:388
msgid "Ratio Height"
msgstr "Seitenverhältnis Höhe"

#: converter/gtk/window.blp:408
msgid "Converting…"
msgstr "Umwandlung läuft …"

#: converter/gtk/window.blp:409
msgid "This could take a while."
msgstr "Das kann eine Weile dauern."

#: converter/gtk/window.blp:419 converter/window.py:518
msgid "Loading…"
msgstr "Ladevorgang …"

#: converter/gtk/window.blp:425 converter/window.py:784
msgid "_Cancel"
msgstr "A_bbrechen"

#: converter/gtk/window.blp:451
msgid "Open File…"
msgstr "Datei öffnen …"

#: converter/gtk/window.blp:458
msgid "Show Less Popular Datatypes"
msgstr "Weniger gängige Datentypen anzeigen"

#: converter/gtk/window.blp:470
msgid "Keyboard Shortcuts"
msgstr "Tastenkürzel"

#: converter/gtk/window.blp:475
msgid "About Converter"
msgstr "Info zu Converter"

#: converter/main.py:93
msgid "Code and Design Borrowed from"
msgstr "Code und Design entlehnt von"

#: converter/main.py:104
msgid "Sample Image from"
msgstr "Beispielbild von"

#: converter/main.py:143
msgid ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Irénée Thirion <irenee.thirion@e.email>\n"
"Mattia Borda <mattiagiovanni.borda@icloud.com>\n"
"Heimen Stoffels\n"
"Sergio <sergiovg01@outlook.com>\n"
"Åke Engelbrektson <eson@svenskasprakfiler.se>\n"
"Sabri Ünal <libreajans@gmail.com>\n"
"Azat Zinnetullin"
msgstr ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Irénée Thirion <irenee.thirion@e.email>\n"
"Mattia Borda <mattiagiovanni.borda@icloud.com>\n"
"Heimen Stoffels\n"
"Sergio <sergiovg01@outlook.com>\n"
"Åke Engelbrektson <eson@svenskasprakfiler.se>\n"
"Sabri Ünal <libreajans@gmail.com>\n"
"Azat Zinnetullin"

#: converter/window.py:642 converter/window.py:725
msgid "Converting Cancelled"
msgstr "Umwandlung abgebrochen"

#: converter/window.py:777
msgid "Stop converting?"
msgstr "Umwandlung stoppen?"

#: converter/window.py:778
msgid "You will lose all progress."
msgstr "Sie werden den gesamten Fortschritt verlieren."

#: converter/window.py:785
msgid "_Stop"
msgstr "_Stopp"

#: converter/window.py:818
msgid "Image converted"
msgstr "Bild wurde umgewandelt"

#: converter/window.py:819
msgid "Open"
msgstr "Öffnen"

#: converter/window.py:824
msgid "Error while processing"
msgstr "Fehler bei der Verarbeitung"

#: converter/window.py:850
msgid "Error copied to clipboard"
msgstr "Fehler in die Zwischenablage kopiert"

#: converter/window.py:854
msgid "_Copy to clipboard"
msgstr "In die Zwischenablage _kopieren"

#: converter/window.py:856
msgid "_Dismiss"
msgstr "_Verwerfen"

#~ msgid "Added batch processing and support for GIF, PDF, TIFF, and ICO."
#~ msgstr ""
#~ "Stapelverarbeitung und Unterstützung für GIF, PDF, TIFF und ICO "
#~ "hinzugefügt."

#~ msgid "…or just drop it"
#~ msgstr "… oder einfach hier ablegen"

#~ msgid "Drop the image to load it"
#~ msgstr "Das Bild ablegen, um es zu laden"
